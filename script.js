//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Використання конструкції try...catch можливе при роботі клієнтського програмного забезпечення (ПЗ),
// яке працює з сервером. ПЗ виконує запит, і повинно отримати відповідь. Відповідь має бути певного формату.
// У випадку, якщо відповідь сервера не буде точно відповідати формату (через будь-яку причину: наприклад,
// помилка в базі даних, або зміна API - клієнт не знає, що "правила гри" змінились), обробка результату
// на стороні клієнтського ПЗ буде призводити до помилок. Розробник може передбачити відсутність даних або можливу
// зміну формату відповіді і обробити помилки так, щоб зберегти функціональність свого ПЗ. Також в розробника є можливість
// "прокинути" невідому помилку далі і обробити її за допомогою якоїсь зовнішньої конструкції.
// Ще один приклад - нижче. З допомогою конструкції try...catch реалізований вивід масиву books в консоль.

let root = document.getElementById("root");
let ul = document.createElement("ul");

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    },
    { 
     author: "Е.П. Шевчук",
     name: "Очищение и лечение спиртовыми настойками, наливками, ликерами",
     price: 150
    }, 
  ];



// try {
//     let i = 0;
//     for(let object of books){
//         i++;
//         let li = document.createElement("li");
//         if(!object.author){
//             throw new SyntaxError("No 'name' property in object!");
//         }
//         else {
//             li.textContent = `${i}. Author: ${object.author}`;
//         }
//         ul.append(li);
//     }
// } catch (error) {
//     console.log(error.message);
// }


    let i = 0;

    for(let object of books){

        try {

            let li = document.createElement("li");
    
            if(!object.author){
                throw new SyntaxError("No 'author' property in object!");
            }
            else {
                li.textContent += `Автор: ${object.author},`;
            }
            if(!object.name){
                throw new SyntaxError("No 'name' property in object!");
            }
            else {
                li.textContent += ` Назва: "${object.name}",`;
            }
            if(!object.price){
                throw new SyntaxError("No 'price' property in object!");
            }
            else {
                li.textContent += ` Ціна: ${object.price} грн.`;
            }

            i++;
            li.textContent = `${i}... ` + li.textContent;
    
            ul.append(li);
        }
        catch (error) {
            console.log(error.message);
        }        
    }

 



  root.append(ul);

